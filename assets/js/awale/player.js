let player1 = {
    id: 1,
    name: "Player 1",
    myCase: document.querySelectorAll('#case1, #case2, #case3, #case4, #case5, #case6'),
    yourCase: document.querySelectorAll('#case7, #case8, #case9, #case10, #case11, #case12'),
    idScore: "score-player-1",
};

let player2 = {
    id: 2,
    name: "Player 2",
    myCase: document.querySelectorAll('#case7, #case8, #case9, #case10, #case11, #case12'),
    yourCase: document.querySelectorAll('#case1, #case2, #case3, #case4, #case5, #case6'),
    idScore: "score-player-2",
};

let actualPlayer = player1;

/**
 * @description Retourne le joueur actuelle
 * @returns {{idScore: string, myCase: NodeListOf<Element>, name: string, id: number, yourCase: NodeListOf<Element>}}
 */
export function getActualPlayer() {
    return actualPlayer;
}

/**
 * @description switch de joueur en fin de tour
 */
export function switchPlayer() {
    actualPlayer = actualPlayer.id === 1 ? player2 : player1;

    actualPlayer.yourCase.forEach(function(element){
        element.style.opacity = '0.5';
        element.style.cursor = 'not-allowed';
    });

    actualPlayer.myCase.forEach(function(element){
        element.style.opacity = '1';
        element.style.cursor = 'pointer';
    });
}