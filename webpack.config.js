const path = require('path');

module.exports = {
    mode: 'development',
    entry: {
        app: './assets/js/app.js',
    },
    output: {
        path: path.resolve(__dirname,'public', 'build'),
        filename: 'app.js',
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            }

        ]
    },
}