/**
 * @description Vérifie si une case est vide
 * @param caseElement
 * @returns {boolean}
 */
export function checkPawnExist(caseElement) {
    let checkPawn = caseElement.childElementCount !== 0 ? true : false;
    return checkPawn;
}

/**
 * @description Vérifie si les cases du joueur suivant sont vide
 * @param player
 * @returns {boolean}
 */
export function checkLineVide(player) {
    let elements = document.getElementsByClassName('case');
    let lenght = 12;
    let index = 6;
    let arrayCheck = 0;

    if (player === 2) {index = 0;lenght = 6;}

    for (let i = index; i < lenght; i++) {
        if (checkPawnExist(elements[i]) === false) {
            arrayCheck++;
        }
    }

    let result = arrayCheck < 6 ? true : false;

    return result;
}

/**
 * @description Vérifie si le joueur clique bien sur sa ligne
 * @param element
 * @param player
 * @returns {boolean}
 */
export function  checkLinePlayer(element, player) {
    let attribut = element.getAttribute('id');
    if (player === 1 && (attribut === 'case1' || attribut === 'case2' || attribut === 'case3' || attribut === 'case4' || attribut === 'case5' || attribut === 'case6')) {
        return true;
    }

    if (player === 2 && (attribut === 'case7' || attribut === 'case8' || attribut === 'case9' || attribut === 'case10' || attribut === 'case11' || attribut === 'case12')) {
        return true;
    }
    return false;
}


//return true si tu es sur une case adverse
export function tabAdverse(thisCase,yourTab){
    let result = false;
    yourTab.yourCase.forEach((currentCase)=>{
        if(thisCase.isSameNode(currentCase)){
            result = true;
            return result;
        }
            
    })
    return result;
}

export function twoOrThreePawns(thisCase){
    if((thisCase.childElementCount === 2) || (thisCase.childElementCount === 3)){
        return true;
    }
    return false;
}

/**
 * @description Vérifie que le joueur en jeu puisse jouer si le second joueur à ses cases vides
 * @param caseElement
 * @param elementLinePlayer
 */
export function givePoor(caseElement, currentPlayer) {
    
    if (checkLineVide(currentPlayer.id) === false) { // Vérifie si le joueur suivant
        let array = Array();
        currentPlayer.myCase.forEach(element => array.push(element.getAttribute('id')));
        let nextNumberCase = 6 - (array.indexOf(caseElement.getAttribute('id')) + 1);
        let numberPawnCase = caseElement.childElementCount;
        let result = nextNumberCase < numberPawnCase ? true : false; // true = la partie continue, false = fin de partie
        return result;

    }
    return true
}

export function endPlay(player) {
    let score = document.getElementById(player.idScore).childElementCount;
    //if checkLineVide or score > 24 return true(stop party)
    if (!checkLineVide || score > 24) {
        return true;
    }
    return false;
}

