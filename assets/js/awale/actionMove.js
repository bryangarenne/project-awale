import {checkPawnExist,checkLinePlayer,checkLineVide, tabAdverse, twoOrThreePawns,givePoor, endPlay } from './validators';
import {getActualPlayer, switchPlayer} from './player';

import imgWin from '../../image/img-win.jpg';
import imgNul from '../../image/Logo_3000.jpg';
//const IMG_VICTORY = 
//const IMG_NULL = 


/**
 * @description move pawns 
 * @param {NodeElement} caseClicked 
 */

export function movePawn(caseClicked,player) {
    
    if(!givePoor(caseClicked,player)){
        console.log('pas le droit d affamer l adversaire')
        return
    }
    let copyCaseClicked = caseClicked;
    const countChildElement = caseClicked.childElementCount;
    //loop with the number of pawns in case click
    for (let i = 1; i <= countChildElement; i++) {
        let nextCase = copyCaseClicked.nextElementSibling;
        
        //comeback to the first case
        if(nextCase === null){  
            nextCase = caseClicked.parentElement.firstElementChild;
        }
        //if the case owns 12 or 24 or 36 or 48 pawns, don't add pawn in origin click case
        if( (i === 12) || (i === 23) || (i === 34) || (i === 45) ){
            nextCase = nextCase.nextElementSibling;
        }
        //move the pawn in next case
        nextCase.appendChild(caseClicked.firstElementChild);
        copyCaseClicked = nextCase; 
    }
    if(tabAdverse(copyCaseClicked,player) && twoOrThreePawns(copyCaseClicked)){
        do{
            eatPawn(copyCaseClicked,player);
            if(copyCaseClicked.previousElementSibling === null){  
                copyCaseClicked = copyCaseClicked.parentElement.lastElementChild;
            }
            copyCaseClicked = copyCaseClicked.previousElementSibling
            
        }while(tabAdverse(copyCaseClicked,player) && twoOrThreePawns(copyCaseClicked))
    }
    if(endPlay(player)){
        let win = winnerIs(player)
        pageWin(win)
    }
    switchPlayer()

}



export function eatPawn(caseClicked,player){
    const countChildElement = caseClicked.childElementCount;
    for (let i = 1; i <= countChildElement; i++) {
       const divScore =  document.getElementById(player.idScore);
       divScore.appendChild(caseClicked.firstElementChild)
    }
}



function winnerIs(player){
    //determiner le player
    //comparer les deux div de score
    let scorePlayer1 = document.getElementById('score-player-1').childElementCount;
    let scorePlayer2 = document.getElementById('score-player-2').childElementCount;
    let winner;
    if(scorePlayer1 > scorePlayer2){
        if (player.id === 1 ){
           winner = {
               name:`Victoire pour ${player.name}`,
               img: `./build/${imgWin}`,
               alt: "image d'un bébé avec le pouce en l'air" 
            }
        }else{
            switchPlayer()
            winner = {
                name:`Victoire pour ${player.name}`,
                img: `./build/${imgWin}`,
                alt: "image d'un bébé avec le pouce en l'air" 
            }
        }
         
    }else if(scorePlayer1 === scorePlayer2){
        winner = {
            name: "Match nul!!!",
            img: `./build/${imgNul}`,
            alt: "image avec écrit: match nul"
        }
    }else{
        if (player.id === 2 ){
            winner = {
                name:`Victoire pour ${player.name}`,
                img: `./build/${imgWin}`,
                alt: "image d'un bébé avec le pouce en l'air" 
             }
         }else{
            switchPlayer()
            winner = {
                name:`Victoire pour ${player.name}`,
                img: `./build/${imgWin}`,
                alt: "image d'un bébé avec le pouce en l'air" 

            }
         }
    }
    return winner;

}


function pageWin(winnerIs){
    let html = `
        <div>
            <h1>${winnerIs.name}</h1>
            <img src="${winnerIs.img}" alt=${winnerIs.alt}>
        </div>
    `;
    document.body.innerHTML = html;
}


